# Fachschaft Informatik Uni Frankfurt Erstsemesterinfos

Hallo liebe Informatikbegeisterte!

Dieses Repo ist eine Sammlung von Infos, die die Fachschaft Informatik für euch
in den Tagen der Ausgangsbeschränkungen zusammengestellt hat.
Wir hoffen ihr findet hier einige Informationen, die euch den Einstieg in dieses
weitestgehend digitale Semester erleichtern.

Eine Frage ist trotz der ganzen Texte untergegangen? Du möchtest Mitstudierende,
Fachschaftler*innen und/oder Studierende aus höheren Semestern kennen lernen?
Dann komm doch in unsere 

* **Telegram Gruppe:** https://t.me/informatikinfo

oder komm auf unseren

* **Discord Server:** https://discord.gg/ttRpw25

Irgendjemand wird da schon online sein und ein offenes Ohr für deine Fragen haben.
Außerdem lässt sich so bestimmt auch jemand finden, der einen Übungszettel gemeinsam
machen möchte.

Du hast weder Telegram, noch Discord oder willst uns bei Fragen lieber per Mail
kontaktieren? Schreib uns eine **Mail** über das Kontaktformular auf unserer Webseite: 

* https://fs.cs.uni-frankfurt.de/contact#

Unser auf **Twitter** könnt ihr uns auch folgen (bitte keine Fragen wegen Hilfe dort):

* @fachschaftinfo

Außerdem empfehlen wir den Discord Channel des Ingo-Wegener-Lernzentrums, der zusätzlich
aktuelle Informationen zu den meisten Basismodulen bereitstellt. Siehe dazu die Seite
des Lernzentrums: 
* http://www.cs.uni-frankfurt.de/index.php/de/service-fuer-studierende/lernzentrum.html

Der Vollständigkeit halber möchten wir noch auf zwei Facebook-Gruppen hinweisen, wo wir
sporadisch noch Veranstaltungsankündigungen für Social Events der Fachschaft Informatik
verbreiten, ansonsten aber zur Zeit nicht weiter nutzen:

* https://www.facebook.com/groups/goethe.informatik/
* https://www.facebook.com/FSInf.Frankfurt


# Was sind diese ganzen Dokumente da?
Ganz einfach:
* **9CP Varianten**: Im Studienverlaufsplan sind bei Sommersemesterersties 9CP im
                    "Anwendungsfach" aufgeführt. Wie ihr die andersweitig füllen könnt
                    haben wir euch hier aufgezählt.
* **Anmeldungen:**  Als Erstsemester sich jetzt zurecht zufinden ist schwierig. Vorallem
                    weil ja man garnicht irgendwo aufkreuzen kann, sondern direkt alles
                    online machen muss. In diesem PDF sammeln wir alle Informationen,
                    die wir zu den Anmeldungen und ersten Vorlesungen haben. Das meiste
                    stammt aus den Online Portalen der Uni. Leider wissen wir auch
                    nicht viel mehr als da steht. ~~Deswegen aktualisieren wir
                    das Dokument immer, wenn wir etwas neues herausfinden.~~
                    Schaut auf die oben verlinkten Seiten von Phillip und Patrik.
                    Die beiden aktualisieren dieselben Infos schneller und besser 
                    für euch aufbereitet als wir ^^
* **Don't Panic:** Unsere Erstsemesterzeitschrift. Die wird normalerweise in Print-Form 
                während unserer Orientierungsveranstaltung (auch OE genannt)
                herausgegeben. Nunja. Die ist leider ins Wasser gefallen, soll
                aber noch nachgeholt werden. Die Don't Panic gibt es aber trotzdem
                schonmal hier. Sie beinhaltet in Kurzform alles wichtige, was ihr
                zu eurem Studium wissen solltet. Ein Blick lohnt sich also!
* **IT-Plattformen:**   Eine Zusammenfassung aller wichtigen Online Systeme unserer Uni.
                        Wie funktioniert das QIS oder die E-Learning Plattformen (z.B. OLAT, Moodle)?
                        Was bietet die RBI an und wo ihr eure Uni Mail überprüfen könnt,
                        sind nur ein paar der Fragen, die hier beantwortet werden.
* **Tipps und Tricks:** Eine Sammlung von verschiedenen Tipps und Tricks zum Studium, 
                        die wir Fachschaftler in unserer unendlichen Weisheit über
                        die Jahre angesammelt haben.

# Folgende Infos sind aus dem Sommersemester 2020 und daher ggf. teilweise veraltet:
Zusätzlich zu unserem Material gibt es auch Informationsseiten von Studierenden
aus höherern Semestern, die für Studierende im 1. oder 2. Semester alle Informationen
zusammengetragen haben. Bereitgestellt wurden die Infos für die OE des Sommersemesters
im Jahr 2020. 

Eine Übersicht zusammengestellt von Patrik: 
* https://sevorio.github.io/uniwebzeug/basis20.html

Eine Übersicht über die Zweitsemestermodule zusammengestellt von Phillip:
* https://phseiff.github.io/Goethe-Uni-Info-2020-Zweitsemester-Info/

Viele Module für das zweite Semester von Winteranfängern sollen auch Sommersemesterersties 
belegen. Ein Blick lohnt sich also. Aber Vorsicht: Ihr müsst nicht alles,  was hier gelistet
ist belegen als Sommersemestererstie.
